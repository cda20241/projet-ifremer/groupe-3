from convertisseur import json_to_dict
from fonctions import *


# Affichez les messages

def group3():
    """
    cette fonction affiche les membres du groupe avec la fonction print().

    """

print("Hello World ! ")
print("C.D.A 2023-2024, Groupe 3 : Nicolas, Winona, Ludovic, Dominique\n")


group3()

# appeler en parametre json_to_dict() pour le dictionnaire type = {date:[[],[],[]],[[],[],[]],[[],[],[]]}
# print{json_to_dict()}


# ################ PREMIER POINT #####################
# Donnez la position (x,y) sur la plage, où la marée a atteint le plus haut point
coord_max(json_to_dict())  # ici j'ai appelé le dictionnaire de test que j'ai créer,
# à remplacer par la version finale (wino)
print('')


# Addition des matrices
resultat = addition(json_to_dict())
# Récupération du nombre de relevés
nbr_releve = len(json_to_dict())
# recupération du nombre et de la position des capteurs (point tjrs immergés puis jamais)
nbr_capteurs_immerges = is_element(resultat, nbr_releve)
nbr_capteurs_non_immerges = is_element(resultat, 0)
# Affichage
print("Il y a "+str(len(nbr_capteurs_immerges))+" capteurs toujours immergés:")
print('Le numéro '+position(nbr_capteurs_immerges))
print("Il y a "+str(len(nbr_capteurs_non_immerges))+" capteurs jamais immergés:")
print('Le numéro '+position(nbr_capteurs_non_immerges))
print('')
verif = False
while verif != True:
    try:
        print('')
        print('Veuillez entrer un chiffre entre 1 et 9')
        choix = int(input("Pour obtenir le pourcentage de temps pendant lequel "
                          "chaque point s'est retrouvé dans l'eau, entrez le numéro du capteur:"))
        if 1 <= choix <= 9:
            verif = True
    except Exception as ex:
        print("Entrée incorrecte")
        verif = False
verif = False

while verif != True:
    try:
        calcul_date(json_to_dict(), 0)
        date_debut = int(input(''))
        if 1 <= date_debut <= len(json_to_dict()):
            verif = True
            calcul_date(json_to_dict(), date_debut)
        else :
            print("Entrée incorrecte")
            verif = False
    except Exception as ex:
        print("Entrée incorrecte")
        verif = False



date_fin = int(input(''))
date_1 = recupere_date(date_debut, json_to_dict())
date_2 = recupere_date(date_fin, json_to_dict())
# Affichez le nouveau dictionnaire
new_dico = filtrer_donnees_par_dates(date_1, date_2, json_to_dict())
pourcentage = calcul_pourcentage(choix, new_dico)

print("Le capteur N°" + str(choix) + " est immergé " + str(pourcentage) + " % du temps")
print('Du ' + format_date(date_1) + " au ", format_date(date_2))
print('')
# Affiche la date à laquelle le plus grand nombre de capteurs sont immergés
# (confition = 1 ==> capteur actif)
check_coef(json_to_dict(), 1)
# Affiche la date à laquelle le plus grand nombre de capteurs ne sont pas immergés
# (confition = 0 ==> capteur inactif)
check_coef(json_to_dict(), 0)

print('')
