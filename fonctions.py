from utils import format_date

MATRIX_SIZE = 3  # Taille de la matrice
CONDITION_HIGH_COEF = 1  # Condition pour les valeurs (valeur égale à 1)
CONDITION_LOW_COEF = 0  # Condition pour les valeurs (valeur égale à 0)


def check_coef(data, condition):
    """
    This function shows the date on which the tide's coefficient is the highest or the lowest depending on the condition

    :param data: dictionary of dates : values (in a matrix)

    :param condition: 0 for lowest coefficient, 1 for highest coefficient

    :return: none
    """
    max_sum = float('-inf')  # Initialisation de la somme maximale
    key_max_sum = None  # Initialisation de la clé correspondante

    for key, value in data.items():
        if (isinstance(value, list) and len(value) == MATRIX_SIZE
                and all(all(element == condition or element == (0 if condition == CONDITION_HIGH_COEF else 1)
                            for element in row) for row in value)):
            sum_tmp = sum(sum(1 for element in row if element == condition) for row in value)
            if sum_tmp > max_sum:
                max_sum = sum_tmp
                key_max_sum = key
    try:
        if key_max_sum is not None:
            if condition == CONDITION_HIGH_COEF:
                print('La date à laquelle le plus fort coefficient à été enregistré est le : '
                      + format_date(key_max_sum))
            elif condition == CONDITION_LOW_COEF:
                print('La date à laquelle le plus faible coefficient à été enregistré est le : '
                      + format_date(key_max_sum))
            return key_max_sum
        else:
            print('La date n\'a pas pu être trouvée.')
            return None
    except Exception as ex:
        print(ex)


# ##########TROUVER COORDONNEES DU POINT LE PLUS HAUT##################
def coord_max(dictionnaire):
    """**INPUT** : dictionnaire clé : matrice
    **OUTPUT** : coordonnées du point le lus haut ligne/colonne
    **DESCRIPTION** : Permet de connaître les coordonnées du /des points les plus hauts.
    """

    # ##### Score de la marée #####

    score = {} 

    for cle in dictionnaire:            # Pour chaque clé dans le dictionnaire
        somme_score = 0                 # On initialise le score à 0
        for ligne in dictionnaire[cle]:  # Pour chaque ligne de la clé donnée
            for valeur in ligne:        # Pour chaque valeur de la ligne donnée
                somme_score += valeur   # On ajoute la valeur au score
        score[cle] = somme_score        # ici on associe les clés à leur score en un dictionnaire

    meilleure_cle = max(score, key=score.get)   # retourne la clé où le plus haut score est trouvé

    # #### Accéder aux coordonnées du point le plus haut #####

    num_to_find = 1

    for cle, liste in dictionnaire.items():     # permet d'itérer chaque élément clé, valeur dans le dictionnaire
        if cle == meilleure_cle:
            last_list = liste[-1]   # pour accéder à la dernière liste
            # enumerate permet d'itérer dans cette liste de garder la position dans la liste
            for i, valeur in enumerate(last_list):
                if valeur == num_to_find:
                    print(f"La marée atteint son point le plus haut à la ligne {i}, "
                          f"colonne {last_list.index(num_to_find)} le" + format_date(cle))

# ########################################


pos_capteurs = [[0, 0], [0, 1], [0, 2], [1, 0], [1, 1], [1, 2], [2, 0], [2, 1], [2, 2]]

def addition(data):
    """
    Additionne toutes les matrices de data
    :param data: liste de données
    :return: renvoie une nouvelle matrice résultant de l'addition de toutes les matrices
    """
    resultat = [ligne.copy() for ligne in data[next(iter(data))]]
    for nom_matrice, matrice in data.items():
        if nom_matrice != list(data.keys())[0]:
            for i in range(len(resultat)):
                for j in range(len(resultat[0])):
                    resultat[i][j] += matrice[i][j]
    return resultat


def is_element(matrice, nbr):
    """
    Fonction qui recherche un nombre dans une matrice
    :param matrice: la matrice dans laquelle on va rechercher le chiffre
    :param nbr: nombre de relevé
    :return: si le chiffre existe renvoie la position du chiffre dans la matrice sinon renvoie False
    """
    i = 0
    liste = []
    for x in range(len(matrice)):
        for y in range(len(matrice[0])):
            if matrice[x][y] == nbr:
                i += 1
                liste.append([x, y])
    return liste


def position(data):
    """
    Fonction qui renvoie le N° du capteur selon ses coordonnées X Y
    :param data: liste de coordonnées [x, y]
    :return: Renvoie le ou les numéros des capteurs
    """
    x_to_capteur = {0: 1, 1: 4, 2: 7}  # Mappage de x à numéro de capteur
    numero_capteurs = []
    for x, y in data:
        numero_capteur = x_to_capteur.get(x)
        if numero_capteur is not None:
            numero_capteur += y
            numero_capteurs.append(numero_capteur)
    if numero_capteurs:
        return ' '.join(map(str, numero_capteurs))
    else:
        return ''


def calcul_pourcentage(numero_capteur, data):
    """
        Calcule le pourcentage d'activité d'un capteur donné en fonction de son numéro et des données.

        :param numero_capteur: Le numéro du capteur dont on souhaite calculer le pourcentage d'activité.
        :param data: Un dictionnaire contenant les données des capteurs.

        :return: Le pourcentage d'activité du capteur, ou None si le numéro du capteur n'est pas numérique.
    """
    if str(numero_capteur).isnumeric():
        # On récupère la position du capteur
        pos = (pos_capteurs[int(numero_capteur)-1])
        x = pos[0]
        y = pos[1]
        actif = 0
        for valeur in data.values():
            if valeur[x][y] == 1:
                actif += 1
        calcul = 100/len(data)*actif
        return calcul
    return None


def filtrer_donnees_par_dates(date_1, date_2, dico):
    """
        Filtrer un dictionnaire de données en fonction des dates incluses dans une plage donnée.

        :param date_1: Date de début de la plage (inclus).
        :param date_2: Date de fin de la plage (inclus).
        :param dico: Un dictionnaire de données contenant des matrices associées à des dates.

        :return: Un nouveau dictionnaire contenant uniquement les données dont les dates se
        trouvent dans la plage spécifiée.
        """
    nouveau_dico_datas = {}
    for date, matrice in dico.items():
        if date_1 <= date <= date_2:
            nouveau_dico_datas[date] = matrice
    return nouveau_dico_datas


def calcul_date(data, debut_fin):
    """
       Affiche les dates disponibles dans un dictionnaire de données et propose un choix de sélection.

       :param data: Un dictionnaire de données contenant des informations associées à des dates.
       :param debut_fin: Un entier représentant le choix de début ou de fin de la sélection.
                         Si 0, affiche toutes les dates disponibles pour le choix de fin.
                         Sinon, n'affiche que les dates dont le numéro est inférieur à debut_fin.

       :return: Aucune valeur de retour. La fonction affiche les options de sélection.
    """
    i = 1
    if debut_fin == 0:
        for date in data.keys():
            print(f'Tapez {str(i)} pour sélectionner la date de début du {format_date(date)}')
            i += 1
    else:
        for date in data.keys():
            if int(debut_fin) <= i:
                print(f'Tapez {str(i)} pour sélectionner la date de fin du {format_date(date)}')
            i += 1


def recupere_date(numero, data):
    """
        Affiche les dates disponibles dans le dictionnaire de données et propose un choix de sélection.

        :param data: Un dictionnaire de données contenant des informations associées à des dates.

        :param numero: Un entier représentant le choix de début ou de fin de la sélection.
                          Si 0, affiche toutes les dates disponibles pour le choix de fin.
                          Sinon, n'affiche que les dates dont le numéro est inférieur ou égal à debut_fin.

        :return: Aucune valeur de retour. La fonction affiche les options de sélection.
    """
    i = 1
    for date in data.keys():
        if int(numero) == i:
            return date
        else:
            i += 1


def verification_input(nbr, mini, maxi):
    """
       Vérifie si un nombre se trouve dans une plage donnée.

       :param nbr: Le nombre à vérifier.
       :param mini: La valeur minimale de la plage.
       :param maxi: La valeur maximale de la plage.

       :return: True si le nombre est un chiffre et se trouve dans la plage [mini, maxi] (inclus), sinon False.
    """
    if nbr.isdigit():
        if mini <= int(nbr) <= maxi:
            return True
        else:
            return False
    else:
        return False
